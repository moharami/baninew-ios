import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'flex-end',
        position: 'relative',

    },
    image: {
        width: '100%',
        height: '20%',
        // height: '36%'
        // resizeMode: 'contain',
        backgroundColor: 'rgb(61, 99, 223)',
    },
    TrapezoidStyle: {
        flex: 1,
        position: 'absolute',
        bottom: '78%',
        right: 0,
        left: 0,
        zIndex: 100,

        borderTopWidth: 50,
        borderLeftWidth: 0,
        borderBottomWidth: 0,
        borderTopColor: 'rgb(61, 99, 223)',
        borderRightColor: 'rgb(233, 233, 233)',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
    },
    topBlue: {
        backgroundColor: 'rgb(61, 99, 223)',
        width: '100%',
        height: 60,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: -1,
    },
    imageRow: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'flex-end',
        paddingTop: 20
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        width: 150,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: '20%'
    },
    name: {
        fontSize: 15,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10
    },
    scroll: {
        position: 'absolute',
        flex: 1,
        top: '17%',
        right: 0,
        left: 0,
        bottom: 0,
        zIndex: 9999,
        marginBottom: 60
    },
    body : {
        // paddingTop: '38%',
        alignItems: 'center',
        justifyContent: 'center',
        padding: 20,


    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
