import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, Picker, AsyncStorage, BackHandler, Alert, ImageBackground, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import IIcon from 'react-native-vector-icons/dist/Ionicons';
import FIcon from 'react-native-vector-icons/dist/Feather';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios';
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from "../../components/homeHeader/index";
import SwitchRow from '../../components/switchRow';
import Slider from "react-native-slider";
import Selectbox from 'react-native-selectbox'
import AlertView from '../../components/modalMassage'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import SnapSlider  from 'react-native-snap-slider'
import bani_bg  from '../../assets/bani_bg.png'
import SelectInput from 'react-native-select-input-ios'

class OtherVacleInfo extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: false,
            loading2: false,
            input1: this.props.travel || this.props.medical  ? 0: 0,
            input1Title:null,
            input2: 0,
            input2Title:null,
            input3:0,
            input3Title:null,
            input4:0,
            input4Title:null,
            input5:  this.props.medical  ? 1 : 0,
            // input5:0,
            input5Title:null,
            inputTxt1: '',
            inputTxt2: '',
            inputTxt3: '',
            earthquake: false,
            pipe: false,
            earth: false,
            rain: false,
            tornado: false,
            flood: false,
            airplane: false,
            steal: false,
            area_price: 1,
            sliderChange: false,
            countries: [],
            age: 7,
            ageTitle:null,
            expert: [],
            feature: [],
            filter: 'point-down',
            modalVisible: false,
            redBorder: false,
            visibleinput1: false,
            visibleinput2: false,
            // birthdayYear: '',
            // birthdayMonth: '',
            // birthdayDay: '',
            // birthdayYear: this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday !== "null" ?  this.props.user.birthday.split('-')[0] : 0,
            // birthdayMonth: this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday !== "null" ? this.props.user.birthday.split('-')[1] : 0,
            // birthdayDay: this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday !== "null" ? this.props.user.birthday.split('-')[2] : 0,
            expertItems: [],
            sliderOptions: [],
            birthdayYear:  this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[0] : 0,
            birthdayMonth: this.props.user.birthday  && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[1] : 0,
            birthdayDay: this.props.user.birthday  && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[2] : 0,

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    // sliderOptions = [
    //     {value: 1, label: '1'},
    //
    // ];
    slidingComplete(itemSelected) {
        console.log("slidingComplete");
        console.log("item selected(from callback)" + itemSelected);
        // console.log("item selected " + this.refs.slider.state.item);
        // console.log("value " + this.sliderOptions[this.refs.slider.state.item].value);
        console.log(this.state.sliderOptions[itemSelected]);
        const item = this.state.sliderOptions[itemSelected];
        this.setState({area_price: item.value}, () => {this.test();})

    };

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        // this.setState({loading: true});
        let sliderOptions = [];
        for(let i=1; i<2.1; i+=.1) {
            sliderOptions.push({value: Number((i).toFixed(1)), label: Number((i).toFixed(1))})
        }
        sliderOptions.push({value: 3, label: 3});
        sliderOptions.push({value: 4, label: 4});
        sliderOptions.push({value: 5, label: 5});

        console.log({sliderOptions})
        this.setState({sliderOptions: sliderOptions})
        if(this.props.travel) {
            this.setState({loading: true});
            Axios.get('/request/get-country').then(response => {
                console.log('coun', response.data.data)
                this.setState({loading: false, countries: response.data.data});
            })
                .catch((error) => {
                    // Alert.alert('','خطا')
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                    console.log(error);
                });
        }
        if(this.props.medical) {
            this.setState({loading: true});
            Axios.get('/request/get-expert').then(response => {
                console.log('allexperts', response.data.data)
                this.setState({loading: false, expert: response.data.data});
            })
                .catch((error) => {
                    // Alert.alert('','خطا')
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                    console.log(error);
                });
        }
        // else if(this.props.medical) {
        //     this.setState({loading: true});
        //     Axios.get('/request/get-expert').then(response => {
        //         this.setState({loading: false, expert: response.data.data});
        //     })
        //         .catch((error) => {
        //             // Alert.alert('','خطا')
        //             // this.setState({loading: false});
        //             this.setState({modalVisible: true, loading: false});
        //
        //             console.log(error);
        //         });
        // }
    }
    getExperts(id) {
        console.log('iddddd', id)
        const items = this.state.expert.filter((item) => {return item.type === parseInt(id) });
        let exp = [{key: 0, label: 'انتخاب نشده', value: 0}]
        exp = items.map((item , index)=> {return {key: item.id, label: item.title, value: item.id}});
        this.setState({expertItems: exp})
        // const label = this.state.input2;
        // console.log('label send', this.state.input2.key)
        // this.setState({loading2: true});
        // Axios.post('/request/expertise', {id: this.state.input2.key}).then(response => {
        //     this.setState({loading2: false, expert: response.data});
        // })
        // .catch((error) => {
        //     this.setState({modalVisible: true, loading2: false});
        //     console.log(error);
        // });
    }
    test() {
        if(this.props.fire) {
            if( this.state.input1 !== 0 && this.state.input2 !== 0  && this.state.inputTxt1 !== ''&& this.state.inputTxt2 !== '' && this.state.inputTxt3 !== '') {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.travel) {
            if( this.state.input1 !== 0 && this.state.input2 !== 0  && this.state.input3 !== 0 && this.state.age !== 7) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.medical) {
            if( this.state.input1 !== 0 && this.state.input2 !== 0  && this.state.input3 !== 0 && this.state.input4 !== 0) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.individual) {
            if( this.state.input1 !== 0 && this.state.input4 !== 0  && this.state.birthdayYear !== 0 && this.state.birthdayMonth !== 0 && this.state.birthdayDay !== 0) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
        if(this.props.age) {
            if(this.state.input1 !== 0 && this.state.input2 !== 0  && this.state.input3 !== 0) {
                this.setState({ nextStep: true });
            }
            else {
                this.setState({ nextStep: false})
            }
        }
    }
    closeModal(title) {
        this.setState({modalVisible: false, type: title});
    }
    nextStep() {
        if(this.state.nextStep ) {
            this.setState({loading: true});
            // AsyncStorage.getItem('token').then((info) => {
            // const newInfo = JSON.parse(info);
            // const token = newInfo.token;
            // const id = newInfo.user_id;
            let user_details = {
                user_id: null,
                _token: null,
                fname:null,
                lname:null,
                national_id:null,
                birthday:null,
                state:null,
                city:null,
                tel:null,
                post_code:null,
                address:null,
                reciver:null,
                new_state:null,
                new_city:null,
                new_tel:null,
                new_post_code:null,
                new_address:null,
                new_reciver:null,
                mobile: null,
                new_mobile:null,
                date_0: null,
                date_1:null,
                time_delivery:null,
                date_2:null,
                address_selected:null,
                lat:null,
                lng:null
            };
            if(this.props.fire) {
                let factor = {
                    user_id: null,
                    fire_home_type: this.state.input1,
                    fire_structure: this.state.input2,
                    fire_home_count: this.state.inputTxt2,
                    fire_home_price: this.state.inputTxt1,
                    fire_meters: this.state.inputTxt3,
                    insurance_id: null,
                    earthquake: this.state.earthquake,
                    pipe: this.state.pipe,
                    earth: this.state.earth,
                    rain: this.state.rain,
                    tornado: this.state.tornado,
                    flood: this.state.flood,
                    airplane: this.state.airplane,
                    steal: this.state.steal,
                    area_price: this.state.area_price,
                    sort:this.state.filter,
                    before:null,
                    one:null,
                    two:null,
                    dataSelect:{
                        fire_home_type: this.state.input1Title,
                        fire_structure: this.state.input2Title,
                        fire_home_count: this.state.inputTxt2,
                        fire_home_price: this.state.inputTxt1,
                        fire_meters: this.state.inputTxt3,
                        feature: this.state.feature,
                        fianl_price: null
                    }
                };
                Axios.post('/request/req_fire', {
                    fire_home_type: this.state.input1,
                    fire_home_count: this.state.inputTxt2,
                    fire_structure: this.state.input2,
                    fire_home_price: this.state.inputTxt1,
                    fire_meters: this.state.inputTxt3,
                    insurance_id: null,
                    earthquake: this.state.earthquake,
                    pipe: this.state.pipe,
                    earth: this.state.earth,
                    rain: this.state.rain,
                    tornado: this.state.tornado,
                    flood: this.state.flood,
                    airplane: this.state.airplane,
                    steal: this.state.steal,
                    area_price: this.state.area_price,
                    sort: this.state.filter
                }).then(response=> {
                    this.setState({loading: false});
                    // if(this.state.feature !== null) {
                    //     let str = "";
                    //     this.state.feature.map((item)=>  str = str + item +", ")
                    //     factor.dataSelect.feature = str;
                    // }
                    console.log('fire response',response.data.data)
                    Actions.prices({openDrawer: this.props.openDrawer, bime: Object.values(response.data.data), pageTitle: 'قیمت ها', factor: factor, user_details: user_details, insurType: 'fire', earthquake: this.state.earthquake,
                        pipe: this.state.pipe,
                        earth: this.state.earth,
                        rain: this.state.rain,
                        tornado: this.state.tornado,
                        flood: this.state.flood,
                        airplane: this.state.airplane,
                        steal: this.state.steal})
                })
                    .catch((error) => {
                        console.log(error.response)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
            }
            if(this.props.travel) {
                let factor = {
                    user_id: null,
                    travel_country: this.state.input1,
                    travel_type: this.state.input2,
                    travel_age: this.state.age,
                    travel_time: this.state.input3,
                    insurance_id:null,
                    area_price:null,
                    price:null,
                    sort:this.state.filter,
                    before:null,
                    one:null,
                    two:null,
                    insurance:{
                        id:null,
                        user_id:null,
                        title:null,
                        description:null,
                        content:null,
                        punishment_danger:null,
                        punishment_off:null,
                        punishment_old:null,
                        punishment_vat:null,
                        tax:null,
                        vat:null,
                        off:null,
                        slug:null,
                        custom_danger:null,
                        extra_life:null,
                        person_danger:null,
                        round_it:null,
                        power:null,
                        branches:null,
                        customer_rate:null,
                        delay:null,
                        center_rank:null,
                        summary_rank:null,
                        shop_share:null,
                        complain_rank:null,
                        display:null,
                        deleted_at:null,
                        created_at:null,
                        updated_at:null,
                        attachments:[]
                    },
                    dataSelect:{
                        insurance:{
                            id:null,
                            user_id:null,
                            title:null,
                            description:null,
                            content:null,
                            punishment_danger:null,
                            punishment_off:null,
                            punishment_old:null,
                            punishment_vat:null,
                            tax:null,
                            vat:null,
                            off:null,
                            slug:null,
                            custom_danger:null,
                            extra_life:null,
                            person_danger:null,
                            round_it:null,
                            power:null,
                            branches:null,
                            customer_rate:null,
                            delay:null,
                            center_rank:null,
                            summary_rank:null,
                            shop_share:null,
                            complain_rank:null,
                            display:null,
                            deleted_at:null,
                            created_at:null,
                            updated_at:null,
                            attachments:[]
                        },
                        travel_age:this.state.ageTitle,
                        travel_time:this.state.input3Title,
                        travel_type:this.state.input2Title,
                        travel_country:this.state.input1Title,
                        fianl_price: null
                    }
                };
                console.log('travel_age', this.state.age);
                console.log('travel_type', this.state.input2);
                console.log('travel_time', this.state.input3);
                console.log('travel_country', this.state.input1);

                Axios.post('/request/req_travel', {
                    travel_country: this.state.input1,
                    travel_type: this.state.input2,
                    travel_age: this.state.age,
                    travel_time: this.state.input3,
                    sort: this.state.filter
                }).then(response=> {
                    this.setState({loading: false});
                    console.log('travel response',response.data.data)
                    Actions.prices({openDrawer: this.props.openDrawer, bime: response.data.data, pageTitle: 'قیمت ها', factor: factor,  user_details: user_details, insurType: 'travel'})
                })
                    .catch((response) => {
                        console.log(response.response)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
            }
            if(this.props.medical) {
                let factor = {
                    user_id:null,
                    res_cost:this.state.input1,
                    res_job:this.state.input3,
                    res_time:this.state.input4,
                    res_non_damage:this.state.input5,
                    resp_type_text:this.state.input2,
                    res_cost_text:null,
                    res_job_text:null,
                    res_time_text: null,
                    record_selected:null,
                    price:null,
                    insurance_id:null,
                    sort:this.state.filter,
                    before:null,
                    one:null,
                    two:null,
                    dataSelect:{
                        res_cost_text:null,
                        res_job_text:null,
                        res_time_text:null,
                        res_cost:this.state.input1Title,
                        res_job:this.state.input3Title,
                        res_time:this.state.input4Title,
                        res_non_damage:this.state.input5Title,
                        resp_type_text:this.state.input2Title,
                        record_selected:null,
                        insurance_id:null,
                        fianl_price:null
                    }
                };
                console.log('res_cost', this.state.input1)
                console.log('res_job', this.state.input3)
                console.log('res_time', this.state.input4)
                console.log('resp_type_text', this.state.input2 )
                console.log('', )
                Axios.post('/request/req_responsible', {
                    res_cost: this.state.input1,
                    res_job: this.state.input3,
                    res_time: this.state.input4,
                    resp_type_text: this.state.input2,
                    res_non_damage: this.state.input5,
                    sort: this.state.filter
                }).then(response=> {
                    this.setState({loading: false});
                    console.log('travel response',response.data);
                    Actions.prices({openDrawer: this.props.openDrawer, bime: response.data.data, pageTitle: 'قیمت ها', factor: factor, user_details: user_details, insurType:'responsible'  })
                })
                    .catch((error) => {
                        console.log(error)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
            }
            if(this.props.individual) {
                let factor = {
                    user_id: null,
                    insurancer: this.state.input4,
                    resp_type: this.state.input1,
                    insurance_id: null,
                    age_day: this.state.birthdayDay,
                    age_month: this.state.birthdayMonth,
                    age_year: this.state.birthdayYear,
                    sort:this.state.filter,
                    before:null,
                    one:null,
                    two:null,
                    dataSelect: {
                        // age: this.state.input1 === 2 ? this.state.age : 1,
                        insurancer: this.state.input4Title,
                        resp_type: this.state.input1Title,
                        fianl_price: null,
                        age_day: this.state.birthdayDay,
                        age_month: this.state.birthdayMonth,
                        age_year: this.state.birthdayYear,
                    }
                }
                this.setState({loading: true});
                Axios.post('/request/health', {
                    insurancer: this.state.input4,
                    resp_type: this.state.input1,
                    age_day: this.state.birthdayDay,
                    age_month: this.state.birthdayMonth,
                    age_year: this.state.birthdayYear,
                    sort: this.state.filter,
                    status_installment: 'none',
                    insurance_id:null
                }).then(response=> {
                    this.setState({loading: false});
                    console.log('req_complete response',response.data.data);
                    Actions.prices({openDrawer: this.props.openDrawer, bime: response.data, pageTitle: 'قیمت ها', factor:factor, user_details: user_details, insurType:'complete'})
                })
                    .catch((error) => {
                        console.log(error)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});
                    });
            }
            if(this.props.age) {
                let factor = {
                    user_id: null,
                    life_cost: this.state.input1,
                    life_pay: this.state.input2,
                    life_years: this.state.input3,
                    record_selected: null,
                    price: null,
                    insurance_id: null,
                    sort:this.state.filter,
                    before:null,
                    one:null,
                    two:null,
                    insurance:{
                        id:null,
                        user_id:null,
                        title:null,
                        description:null,
                        content:null,
                        punishment_danger:null,
                        punishment_off:null,
                        punishment_old:null,
                        punishment_vat:null,
                        tax:null,
                        vat:null,
                        off:null,
                        slug:null,
                        custom_danger:null,
                        extra_life:null,
                        person_danger:null,
                        round_it:null,
                        power:null,
                        branches:null,
                        customer_rate:null,
                        delay:null,
                        center_rank:null,
                        summary_rank:null,
                        shop_share:null,
                        complain_rank:null,
                        display:null,
                        deleted_at:null,
                        created_at:null,
                        updated_at:null,
                        attachments:[]
                    },
                    dataSelect:{
                        life_cost: this.state.input1Title,
                        life_pay: this.state.input2Title,
                        life_years: this.state.input3Title,
                        feature: null,
                        fianl_price: null
                    }
                };
                Axios.post('/request/req_life', {
                    life_cost: this.state.input1,
                    life_pay: this.state.input2,
                    life_years: this.state.input3,
                    sort: this.state.filter
                }).then(response=> {
                    this.setState({loading: false});
                    console.log('age response',response.data.data)
                    Actions.prices({openDrawer: this.props.openDrawer, bime: response.data.data, pageTitle: 'قیمت ها', factor: factor,  user_details: user_details, insurType:'life' })
                })
                    .catch((response) => {
                        console.log(response.data.data)
                        // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                        // this.setState({loading: false});
                        this.setState({modalVisible: true, loading: false});

                    });
            }
        }
        else {
            this.setState({redBorder: true});
        }
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    switchButtons(status, id, title) {
        console.log(status)
        if(id === 1) {
            this.setState({
                earthquake: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 2) {
            this.setState({
                pipe: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 3) {
            this.setState({
                earth: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 4) {
            this.setState({
                rain: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 5) {
            this.setState({
                tornado: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 6) {
            this.setState({
                flood: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 7) {
            this.setState({
                airplane: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
        else if(id === 8) {
            this.setState({
                steal: status
            }, () => {
                if(status) {
                    let feature = this.state.feature;
                    feature.push(title)
                    this.setState({feature: feature});
                }
            })
        }
    }
    onShowinput1 = () => {
        this.setState({ visibleinput1: true });
    }

    onSelectInput1 = (picked) => {
        console.log('picked', picked)
        this.setState({
            input1: picked,
            visibleinput1: false
        }, () =>{
            console.log('ppppicked', picked)
            this.state.countries.map((item) => {
                if(picked === item.id) {
                    this.setState({input1Title: item.title}, () => {console.log('iiiinput 1',this.state.input1)});
                }
            })
            this.test();
        })
    }

    onCancelinput1 = () => {
        this.setState({
            visibleinput1: false
        })
    }

    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        console.log('picked', picked)
        this.setState({
            input1: picked,
            visibleinput2: false
        }, () =>{
            console.log('sellllected experts', this.state.expertItems)
            this.state.expertItems.map((item) => {
                if(picked === item.value) {
                    this.setState({input1Title: item.label}, () => {console.log(this.state.input1Title)});
                }
            })
            this.test();
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    getValueLabel(options, value){
        let valueOptions = options;

        const item = valueOptions.filter((option) =>  {return option.value === value})
        return item[0];

    }
    render() {
        const vaclePriceStr = this.state.inputTxt1.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        const metragStr = this.state.inputTxt3 && this.state.inputTxt3.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

        const typeArray = ["انتخاب نشده", "یک واحد در آپارتمان", "یک ساختمان ویلایی", "آپارتمان یا مجتمع"];
        const structureArray = ["انتخاب نشده", "آجری", "فلزی", "بتنی"];

        const payArray = ["انتخاب نشده", "12,750,000 تومان", "8,500,000 تومان", "5,300,000 تومان", "3,200,000 تومان", "1,600,000 تومان", "1,050,000 تومان"];
        const costArray = ["انتخاب نشده", "ماهیانه", "سه ماهه", "شش ماهه", "سالانه"];
        const yearArray = ["انتخاب نشده", "13 ساله", "16 ساله", "20 ساله", "25 ساله"];
        const medicalNumArray = ["انتخاب نشده", "یک دیه", "دو دیه", "سه دیه", "چهار دیه"];
        const medicalTimeArray = ["انتخاب نشده", "یک ساله"];

        const indivTypeArray = ["انتخاب نشده", "تامین اجتماعی", "نیرو های مسلح", "بانک ها"];
        const insurancerIndivTimeArray = ["انتخاب نشده", "انفرادی"];

        const insuranceTypeArray = ["انتخاب نشده", "10,000 یورو (آسیا آفریقا بجز چین و ژاپن)", " 30,000 یورو", "50,000 یورو (شینگن و سایر کشورها بجز چین ، ژاپن ،آمریکا ،کانادا و استرالیا", "50,000 یورو (کلیه کشورها )"];
        const travelArray = ["انتخاب نشده", "1 تا 7 روز", "8 تا 15 روز", "16 تا 23 روز", "24 تا 31 روز", "32 تا 45 روز", "46 تا 62 روز", "63 تا 92 روز", "6 ماه", "1 سال"];

        const ageArray = ["انتخاب نشده", "1-12", "13-65", "66-70", "71-75", "76-80", "81-85"];



        let typeArrayItem = [];
        typeArray.map((item, index) => {typeArrayItem.push({key: index, label: item, value: index})})

        let structureArrayItem = [
            {key: 0, label: "انتخاب نشده", value: 0},
            {key: 1, label: "آجری", value: "bricks"},
            {key: 2, label: "فلزی", value: "metal"},
            {key: 3, label: "بتنی", value: "concrete"},
        ];
        // structureArray.map((item, index) => {structureArrayItem.push({key: index, label: item, value: index})})

        let payArrayItem = [];
        payArray.map((item, index) => {payArrayItem.push({key: index, label: item, value: index})})

        let costArrayItem = [];
        costArray.map((item, index) => {costArrayItem.push({key: index, label: item, value: index})})

        let yearArrayItem = [
            {key: 0, label: "انتخاب نشده", value: 0},
            {key: 1, label: "13 ساله", value: 13 },
            {key: 2, label: "16 ساله", value: 16},
            {key: 3, label: "20 ساله", value: 20},
            {key: 4, label: "25 ساله", value: 25},
        ];

        let medicalNumArrayItem = [];
        medicalNumArray.map((item, index) => {medicalNumArrayItem.push({key: index, label: item, value: index})})

        let medicalTimeArrayItem = [];
        medicalTimeArray.map((item, index) => {medicalTimeArrayItem.push({key: index, label: item, value: item === "یک ساله" ?  index+2 : index})});

        let indivTypeArrayItem = [];
        indivTypeArray.map((item, index) => {indivTypeArrayItem.push({key: index, label: item, value: index})})

        let insurancerIndivTimeArrayItem = [];
        insurancerIndivTimeArray.map((item, index) => {insurancerIndivTimeArrayItem.push({key: index, label: item, value: item === "انفرادی" ? index+1 : index})})

        let insuranceTypeArrayItem = [];
        insuranceTypeArray.map((item, index) => {insuranceTypeArrayItem.push({key: index, label: item, value: index})})

        let travelArrayItem = [
            {key: 0, label: "انتخاب نشده", value: 0},
            {key: 1, label: "1 تا 7 روز", value: "1-7"},
            {key: 2, label: "8 تا 15 روز", value: "8-15"},
            {key: 3, label: "16 تا 23 روز", value: "16-23"},
            {key: 4, label: "24 تا 31 روز", value: "24-31"},
            {key: 5, label: "32 تا 45 روز", value: "32-45"},
            {key: 6, label: "46 تا 62 روز", value: "46-62"},
            {key: 7, label: "63 تا 92 روز", value: "63-92"},
            {key: 8, label: "6 ماه", value: "180"},
            {key: 9, label: "1 سال", value: "360"}
        ];


        let non_damage_items = [
            {key: 0, label: "انتخاب نشده", value:0},
            {key: 1, label: "صدور اولیه یا با خسارت", value: 1},
            {key: 3, label: "1 سال", value: 0.85},
            {key: 4, label: "2 سال", value: 0.75},
            {key: 5, label: "3 سال", value: 0.65},
            {key: 6, label: "4 سال و بیشتر", value: 0.6}
        ];

        let ageArrayItem = [];
        ageArray.map((item, index) => {ageArrayItem.push({key: index, label: item, value: item === "انتخاب نشده" ? 7 : index})})

        // const monthArray = ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر","دی", "بهمن","اسفند"]
        // let dayArray = ["1", "2", "3", "4", "5", "6", "7", "8", "9","10", "11","12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22", "23","24", "25", "26", "27", "28", "29", "30", "31"];
        //
        // const shadowOpt = {
        //     width:100,
        //     height:100,
        //     color:"#000",
        //     border:2,
        //     radius:3,
        //     opacity:0.2,
        //     x:0,
        //     y:3,
        //     style:{marginVertical:5}
        // };

        // let yearArray2 = [], monthArray = [], dayArray = [];
        // let yearArray2 = [{key: 0, label: this.props.user.birthday !== null &&  this.props.user.birthday !== "null" ? this.props.user.birthday.split('-')[0] : 'سال', value: this.props.user.birthday !== null &&  this.props.user.birthday !== "null" ? this.props.user.birthday.split('-')[0] : 0 }], monthArray = [{key: 0, label: this.props.user.birthday !== null &&  this.props.user.birthday !== "null" ? this.props.user.birthday.split('-')[1] : 'ماه', value: this.props.user.birthday !== null &&  this.props.user.birthday !== "null" ? this.props.user.birthday.split('-')[1] : 0}], dayArray = [{key: 0, label: this.props.user.birthday !== null &&  this.props.user.birthday !== "null" ? this.props.user.birthday.split('-')[2] : 'روز', value: this.props.user.birthday !== null &&  this.props.user.birthday !== "null" ? this.props.user.birthday.split('-')[2] : 0}];

        // for(let i=1397; i>=1300 ; i--){
        //     yearArray2.push({key: i, label: i.toString(), value: i})
        // }
        // const month = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12"]
        // monthArray = month.map((item)=> {return {key: parseInt(item), label: item, value: item}})
        // const day = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22", "23","24", "25", "26", "27", "28", "29", "30", "31"];
        // dayArray = day.map((item)=> {return {key: parseInt(item), label: item, value: item}})
        //


        //
        // for(let i=1397, j= 0; i>=1300 ; i--, j++){
        //     yearArray.push({key: j, label: i.toString(), value: i})
        // }
        // const month = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12"]
        // monthArray = month.map((item)=> {return {key: parseInt(item), label: item, value: item}})
        // const day = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22", "23","24", "25", "26", "27", "28", "29", "30", "31"];
        // dayArray = day.map((item)=> {return {key: parseInt(item), label: item, value: item}})


        const {user} = this.props;
        let yearArray2 = [{key: 0, label: this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[0] : 'سال', value: this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[0] : 0 }],
            monthArray = [{key: 0, label: this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[1] : 'ماه', value: this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[1] : 0 }],
            dayArray = [{key: 0, label:  this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[2] :  'روز', value: this.props.user.birthday && this.props.user.birthday !== null && this.props.user.birthday!== "null" ?  this.props.user.birthday.split('-')[2] : 0 }];

        for(let i=1397, j= 0; i>=1300 ; i--, j++){
            yearArray2.push({key: j, label: i.toString(), value: i})
        }
        const month = ["ماه","01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12"]
        monthArray = month.map((item)=> {return {key: parseInt(item), label: item, value: item === "ماه" ? 0 : item}})
        const day = ["روز","01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22", "23","24", "25", "26", "27", "28", "29", "30", "31"];
        dayArray = day.map((item)=> {return {key: parseInt(item), label: item, value: item === "روز" ? 0 :  item}})




        const text11 =  <Text style={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}} >1971</Text>
        if(this.state.loading){
            return (<Loader />)
        } else
            return (
                <View style={styles.container}>
                    <View style={styles.header}>
                        <HomeHeader active={1} pageTitle={this.props.pageTitle} openDrawer={this.props.openDrawer}/>
                    </View>
                    <ScrollView style={styles.scroll}>
                        <View style={styles.bodyContainer}>
                            <View style={{width: '100%', zIndex: 0, paddingBottom: 70}}>
                                {
                                    this.props.fire ?
                                        <View>
                                            <Text style={styles.text}>نوع ملک</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input1}
                                                    options={typeArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input1: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(typeArrayItem, itemValue);
                                                            this.setState({input1Title: title.label}, () => {console.log(this.state.input1Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : ( this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>


                                            <Text style={styles.text}>نوع سازه</Text>

                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input2}
                                                    options={structureArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input2: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(structureArrayItem, itemValue);
                                                            this.setState({input2Title: title.label}, () => {console.log(this.state.input2Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input2 === 0 ? 'red' : ( this.state.input2 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>

                                            <Text style={styles.text}>ارزش لوازم خانگی(تومان)</Text>
                                            <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.inputTxt1 === '' ? 'red' : ( this.state.inputTxt1 !== '' ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                                <TextInput
                                                    onChangeText={(text) =>{
                                                        this.setState({
                                                            inputTxt1: text.toString().replace(/,/g, "")
                                                        }, () => {
                                                            this.test();
                                                        })}}
                                                    keyboardType={"numeric"}
                                                    maxLength={13}
                                                    placeholder='ارزش لوازم خانگی'
                                                    placeholderTextColor={'gray'}
                                                    underlineColorAndroid='transparent'
                                                    // value={this.state.inputTxt1}
                                                    value={vaclePriceStr}
                                                    style={{
                                                        height: 42,
                                                        paddingRight: 25,
                                                        width: '100%',
                                                        color: 'gray',
                                                        fontSize: 14,
                                                        textAlign: 'right'
                                                    }}
                                                />
                                            </View>

                                            <Text style={styles.text}>تعداد واحد</Text>
                                            <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.inputTxt2 === '' ? 'red' : ( this.state.inputTxt2 !== '' ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                                <TextInput
                                                    // onFocus={() => {  this.setState({showPicker: true}); this.test();}}
                                                    onChangeText={(text) =>{
                                                        this.setState({
                                                            inputTxt2: text
                                                        }, () => {
                                                            this.test();
                                                        })}}
                                                    placeholder='تعداد واحد'
                                                    keyboardType={"numeric"}
                                                    maxLength={4}
                                                    placeholderTextColor={'gray'}
                                                    underlineColorAndroid='transparent'
                                                    value={this.state.inputTxt2}
                                                    style={{
                                                        height: 42,
                                                        paddingRight: 25,
                                                        width: '100%',
                                                        color: 'gray',
                                                        fontSize: 14,
                                                        textAlign: 'right'
                                                    }}
                                                />
                                            </View>
                                            <Text style={styles.text}>متراژ مورد بیمه</Text>
                                            <View style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42, borderRadius: 10, borderColor: this.state.redBorder && this.state.inputTxt3 === '' ? 'red' : ( this.state.inputTxt3 !== '' ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1}}>
                                                <TextInput
                                                    // onFocus={() => {  this.setState({showPicker: true}); this.test();}}
                                                    onChangeText={(text) =>{
                                                        this.setState({
                                                            inputTxt3: text.toString().replace(/,/g, "")
                                                        }, () => {
                                                            this.test();
                                                        })}}
                                                    keyboardType={"numeric"}
                                                    placeholder='متراژ مورد بیمه'
                                                    maxLength={10}
                                                    placeholderTextColor={'gray'}
                                                    underlineColorAndroid='transparent'
                                                    value={metragStr}
                                                    style={{
                                                        height: 42,
                                                        paddingRight: 25,
                                                        width: '100%',
                                                        color: 'gray',
                                                        fontSize: 14,
                                                        textAlign: 'right'
                                                    }}
                                                />
                                            </View>
                                            <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'flex-end'}}>
                                                <Text style={{textAlign: 'center', color: 'green', paddingRight: 10}}>{this.state.area_price}</Text>
                                                <Text style={[styles.text, {fontSize: 14}]}>هزینه ساخت هر متر مربع(میلیون تومان): </Text>
                                            </View>
                                            <SnapSlider ref="slider" style={{width: '100%'}}
                                                // itemWrapperStyle={}
                                                        itemWrapperStyle={{paddingRight: 22, paddingLeft: 18}}
                                                        itemStyle={{fontSize: 10 , fontFamily: 'IRANSansMobile(FaNum)'}}
                                                        items={this.state.sliderOptions}
                                                        labelPosition="bottom"
                                                // defaultItem={this.state.defaultItem}
                                                        onSlidingComplete={(itemSelected)=>this.slidingComplete(itemSelected)} />
                                            <Text style={styles.featureLabel}>پوشش های اجباری</Text>
                                            <SwitchRow label="آتش سوزی" switchButtons={() => null} on />
                                            <SwitchRow label="صاعقه" switchButtons={() => null} on />
                                            <SwitchRow label="انفجار" switchButtons={() => null} on />
                                            <Text style={styles.featureLabel}>پوشش های اختیاری</Text>
                                            {
                                                this.state.pipe || this.state.steal ?
                                                    <Text style={{fontSize: 11, color: 'orange',  fontFamily: 'IRANSansMobile(FaNum)'}}>برای پوشش ترکیدگی لوله و سرقت، صدور نهایی بیمه نیازمند بازدید و تایید
                                                        کارشناس
                                                        بیمه است و تنها در محدوده شهر تهران امکان پذیر است.
                                                    </Text> : null
                                            }
                                            <SwitchRow label="زلزله"  switchButtons={(status) => this.switchButtons(status, 1, 'earthquake') } />
                                            <SwitchRow label="ترکیدگی لوله"  switchButtons={(status) => this.switchButtons(status, 2, 'pipe')} />
                                            <SwitchRow label="نشست زمین"  switchButtons={(status) => this.switchButtons(status, 3, 'earth')} />
                                            <SwitchRow label="ضایعات ناشی از برف و باران"  switchButtons={(status) => this.switchButtons(status, 4, 'rain')} />
                                            <SwitchRow label="طوفان"  switchButtons={(status) => this.switchButtons(status, 5, 'tornado')} />
                                            <SwitchRow label="سیل"  switchButtons={(status) => this.switchButtons(status, 6, 'flood')} />
                                            <SwitchRow label="سقوط هواپیما"  switchButtons={(status) => this.switchButtons(status, 7, 'airplane')} />
                                            <SwitchRow label="سرقت مربوط به شکست حرز"  switchButtons={(status) => this.switchButtons(status, 8, 'steal')} />
                                        </View> : null
                                }
                                {
                                    this.props.travel ?
                                        <View>
                                            <Text style={styles.text}>کشور</Text>
                                            <TouchableOpacity onPress={this.onShowinput1} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : ( this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <Text style={{paddingTop: 5, paddingRight: 10, textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading ? "در حال بارگذاری..." : this.state.input1 === 0 ? "انتخاب نشده" : this.state.input1Title}</Text>
                                                <ModalFilterPicker
                                                    visible={this.state.visibleinput1}
                                                    onSelect={this.onSelectInput1}
                                                    onCancel={this.onCancelinput1}
                                                    options={ this.state.countries.map((item) => {return {key: item.id, label: item.title, value: item.id}})}
                                                    placeholderText="جستجو ..."
                                                    cancelButtonText="لغو"
                                                    filterTextInputStyle={{textAlign: 'right',fontFamily: 'IRANSansMobile(FaNum)'}}
                                                    optionTextStyle={{textAlign: 'right', width: '100%', fontFamily: 'IRANSansMobile(FaNum)'}}
                                                    titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                                    filterTextInputContainerStyle={{padding: 15, borderBottomWidth: 1, borderBottomColor: 'gray'}}
                                                />
                                            </TouchableOpacity>

                                            <Text style={styles.text}>نوع بیمه</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input2}
                                                    options={insuranceTypeArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input2: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(insuranceTypeArrayItem, itemValue);
                                                            this.setState({input2Title: title.label}, () => {console.log(this.state.input2Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input2 === 0 ? 'red' : ( this.state.input2 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>


                                            <Text style={styles.text}>مدت اقامت</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input3}
                                                    options={travelArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input3: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(travelArrayItem, itemValue);
                                                            this.setState({input3Title: title.label}, () => {console.log(this.state.input3Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input3 === 0 ? 'red' : ( this.state.input3 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>


                                            <Text style={styles.text}>سن</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.age}
                                                    options={ageArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            age: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(ageArrayItem, itemValue);
                                                            this.setState({ageTitle: title.label}, () => {console.log(this.state.ageTitle)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.age === 7 ? 'red' : ( this.state.age !== 7 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>

                                        </View>: null
                                }
                                {
                                    this.props.medical ?
                                        <View>
                                            <Text style={styles.text}>نوع بیمه</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input2}
                                                    options={[{key: 0, label: "انتخاب نشده", value: 0},{key: 1, label: "پزشکی", value: 1}, {key: 2, label: "پیراپزشکی", value: 2}]}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input2: itemValue
                                                        }, () => {
                                                            const opt = [{key: 0, label: "انتخاب نشده", value: 0}, {key: 1, label: "پزشکی", value: 1}, {key: 2, label: "پیراپزشکی", value: 2}];
                                                            const title = this.getValueLabel(opt, itemValue);
                                                            if(title.key !== 0) {
                                                                this.getExperts(itemValue);
                                                            }
                                                            this.setState({input2Title: title.label}, () => {console.log(this.state.input2Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input2 === 0 ? 'red' : ( this.state.input2 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth: 1
                                                    }}
                                                />
                                            </View>


                                            <Text style={styles.text}>نوع تخصص</Text>
                                            <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative',  zIndex: 3, width: '100%', backgroundColor: 'white', height: 42,  borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : ( this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <Text style={{paddingTop: 5, paddingRight: 10, textAlign: 'right',  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' }}>{this.state.loading2 ? "در حال بارگذاری..." : this.state.input1 === 0 ? "انتخاب نشده" : this.state.input1Title}</Text>
                                                <ModalFilterPicker
                                                    visible={this.state.visibleinput2}
                                                    onSelect={this.onSelectInput2}
                                                    onCancel={this.onCancelinput2}
                                                    options={this.state.expertItems}
                                                    placeholderText="جستجو ..."
                                                    cancelButtonText="لغو"
                                                    filterTextInputStyle={{textAlign: 'right',fontFamily: 'IRANSansMobile(FaNum)'}}
                                                    optionTextStyle={{textAlign: 'right', width: '100%', fontFamily: 'IRANSansMobile(FaNum)'}}
                                                    titleTextStyle={{textAlign: 'right', fontFamily: 'IRANSansMobile(FaNum)'}}
                                                    filterTextInputContainerStyle={{padding: 15, borderBottomWidth: 1, borderBottomColor: 'gray'}}
                                                />
                                            </TouchableOpacity>

                                            <Text style={styles.text}>تعداد دیه</Text>

                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input3}
                                                    options={medicalNumArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input3: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(medicalNumArrayItem, itemValue);
                                                            this.setState({input3Title: title.label}, () => {console.log(this.state.input3Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input3 === 0 ? 'red' : ( this.state.input3 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>


                                            <Text style={styles.text}>مدت بیمه نامه</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input4}
                                                    options={medicalTimeArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input4: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(medicalTimeArrayItem, itemValue);
                                                            this.setState({input4Title: title.label}, () => {console.log(this.state.input4Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input4 === 0 ? 'red' : ( this.state.input4 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>
                                            <Text style={styles.text}>تخفیف عدم خسارت</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input5}
                                                    options={non_damage_items}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input5: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(non_damage_items, itemValue);
                                                            this.setState({input5Title: title.label}, () => {console.log(this.state.input5Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input5 === 0 ? 'red' : ( this.state.input5 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>

                                        </View>: null
                                }
                                {
                                    this.props.individual ?
                                        <View>
                                            <Text style={styles.text}>بیمه گر پایه</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input4}
                                                    options={indivTypeArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input4: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(indivTypeArrayItem, itemValue);
                                                            this.setState({input4Title: title.label}, () => {console.log(this.state.input4Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input4 === 0 ? 'red' : ( this.state.input4 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>

                                            <Text style={styles.text}>نوع بیمه</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input1}
                                                    options={insurancerIndivTimeArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input1: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(insurancerIndivTimeArrayItem, itemValue);
                                                            this.setState({input1Title: title.label}, () => {console.log(this.state.input1Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : ( this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>

                                            <Text style={styles.text}>تاریخ تولد</Text>
                                            <View style={{flexDirection: 'row', padding: 3, zIndex: 3, width: '100%', height: 42, borderColor: this.state.redBorder && (this.state.birthdayYear  === 0 || this.state.birthdayMonth  === 0 || this.state.birthdayDay  === 0) ? 'red' : (this.state.birthdayYear  !== 0 && this.state.birthdayMonth  !== 0 && this.state.birthdayDay  !== 0  ? 'rgb(50, 197, 117)' : '#ccc'), borderWidth: 1, borderRadius: 10,  backgroundColor: 'white'}}>
                                                {/*<View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>*/}
                                                    {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '20%', left: 10}}/>*/}
                                                    <SelectInput
                                                        value={this.state.birthdayYear}
                                                        options={yearArray2}
                                                        onCancelEditing={() => console.log('onCancel')}
                                                        submitKeyText="انتخاب"
                                                        cancelKeyText="لغو"
                                                        labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        onSubmitEditing={(itemValue) =>{
                                                            console.log('itemValue for years', itemValue)
                                                            this.setState({
                                                                birthdayYear: itemValue
                                                            }, () => {
                                                                this.test();
                                                            })}}
                                                        // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                        style={{
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                            width: '33%', height: 42, paddingRight: 10,
                                                        }}
                                                    />
                                                {/*</View>*/}

                                                {/*<View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>*/}
                                                    {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '20%', left: 10}}/>*/}
                                                    <SelectInput
                                                        value={this.state.birthdayMonth}
                                                        options={monthArray}
                                                        onCancelEditing={() => console.log('onCancel')}
                                                        submitKeyText="انتخاب"
                                                        cancelKeyText="لغو"
                                                        labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        onSubmitEditing={(itemValue) =>{
                                                            console.log('itemValue for years', itemValue)
                                                            this.setState({
                                                                birthdayMonth: itemValue
                                                            }, () => {
                                                                this.test();
                                                            })}}
                                                        // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                        style={{
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                            width: '33%', height: 42, paddingRight: 10,
                                                        }}
                                                    />
                                                {/*</View>*/}

                                                {/*<View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>*/}
                                                    {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: '20%', left: 10}}/>*/}
                                                    <SelectInput
                                                        value={this.state.birthdayDay}
                                                        options={dayArray}
                                                        onCancelEditing={() => console.log('onCancel')}
                                                        submitKeyText="انتخاب"
                                                        cancelKeyText="لغو"
                                                        labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                        onSubmitEditing={(itemValue) =>{
                                                            console.log('itemValue for years', itemValue)
                                                            this.setState({
                                                                birthdayDay: itemValue
                                                            }, () => {
                                                                this.test();
                                                            })}}
                                                        // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                        style={{
                                                            alignItems: 'center',
                                                            justifyContent: 'center',
                                                            width: '33%', height: 42, paddingRight: 10,
                                                        }}
                                                    />
                                                {/*</View>*/}
                                            </View>
                                        </View>: null
                                }
                                {
                                    this.props.age ?
                                        <View>
                                            <Text style={styles.text}>حق بیمه</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input1}
                                                    options={payArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input1: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(payArrayItem, itemValue);
                                                            this.setState({input1Title: title.label}, () => {console.log(this.state.input1Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input1 === 0 ? 'red' : ( this.state.input1 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>

                                            <Text style={styles.text}>نحوه پرداخت</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input2}
                                                    options={costArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input2: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(costArrayItem, itemValue);
                                                            this.setState({input2Title: title.label}, () => {console.log(this.state.input2Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input2 === 0 ? 'red' : ( this.state.input2 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>

                                            <Text style={styles.text}>طول مدت قرارداد</Text>
                                            <View style={{position: 'relative', elevation: 5, zIndex: 3, width: '100%'}}>
                                                <FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>
                                                <SelectInput
                                                    value={this.state.input3}
                                                    options={yearArrayItem}
                                                    onCancelEditing={() => console.log('onCancel')}
                                                    submitKeyText="انتخاب"
                                                    cancelKeyText="لغو"
                                                    labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                                    onSubmitEditing={(itemValue) =>{
                                                        console.log('itemValue for years', itemValue)
                                                        this.setState({
                                                            input3: itemValue
                                                        }, () => {
                                                            const title = this.getValueLabel(yearArrayItem, itemValue);
                                                            this.setState({input3Title: title.label}, () => {console.log(this.state.input3Title)});
                                                            this.test();
                                                        })}}
                                                    // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                                    style={{
                                                        flexDirection: 'row',
                                                        alignItems: 'center',
                                                        justifyContent: 'flex-end',
                                                        height: 42,
                                                        padding: 8,
                                                        backgroundColor: '#FFFFFF',
                                                        borderColor: this.state.redBorder && this.state.input3 === 0 ? 'red' : ( this.state.input3 !== 0 ? 'rgb(50, 197, 117)' : '#ccc'),
                                                        borderRadius: 10, borderWidth:  1
                                                    }}
                                                />
                                            </View>

                                        </View>: null
                                }
                            </View>
                            <AlertView
                                closeModal={(title) => this.closeModal(title)}
                                modalVisible={this.state.modalVisible}
                                onChange={() => this.setState({modalVisible: false})}

                                title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                            />
                        </View>
                    </ScrollView>
                    <View style={styles.footer}>
                        <TouchableOpacity style={[styles.iconLeftContainer, {backgroundColor: this.state.nextStep ? 'rgba(255, 193, 39, 1)' : 'rgba(200, 200, 200, 1)' }]} onPress={() => this.nextStep()}>
                            <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                            <Text style={styles.label}>بعدی</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                            <IIcon name="md-close" size={18} color="rgba(17, 103, 253, 1)" />
                        </TouchableOpacity>
                    </View>
                </View>
            );
    }
}
function mapStateToProps(state) {
    return {
        user: state.auth.user,
    }
}
export default connect(mapStateToProps)(OtherVacleInfo);

