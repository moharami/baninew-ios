
import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 1,
        backgroundColor: 'rgb(246, 246, 246)',
        // backgroundColor: 'red'
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        backgroundColor: 'white',
        height: 120,
        position: 'absolute',
        top: 0,
        right: 0,
        left: 0,
        zIndex: 9990
    },
    bodyContainer: {
        paddingBottom: 180,
        paddingRight: 10,
        paddingLeft: 10,
        paddingTop: 30
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        // height: 120,
        position: 'absolute',
        bottom: 30,
        right: 0,
        left: 0,
        zIndex: 9999,
        padding: 15
    },
    iconLeftContainer: {
        backgroundColor: 'rgba(51, 197, 117, 1)',
        // width: '40%',
        // height: 32,
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 5,

    },
    label2: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'white',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15,
    },
    iconRightContainer: {
        backgroundColor: 'white',
        borderRadius: 30,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // padding: 15,
        // paddingRight: 10,
        // paddingLeft: 10,
        elevation: 4,
        width: 40,
        height: 40,
    },
    scroll: {
        paddingTop: 120,
        paddingRight: 15,
        paddingLeft: 15,
    },
    topLabel: {
        paddingLeft: 15,
        paddingRight: 10,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 18,
        paddingTop: 20,
        paddingBottom: 20,
    },
    labelContainer: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        paddingBottom: 20
    },
    regimContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8
    },
    body: {
        paddingRight: 10,
        paddingTop: 15,
        paddingBottom: 15,
        borderTopColor: 'rgb(237, 237, 237)',
        borderTopWidth: 1,
        backgroundColor: 'white',
        borderRadius: 10,
        elevation: 5,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 8
    },
    reductionContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        backgroundColor: 'white',
        padding: 20,
        borderRadius: 10,
        elevation: 5,
        marginBottom: 20
    },
    reduction: {
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-between',
        // borderTopColor: 'lightgray',
        // borderTopWidth: 1,
        paddingTop: 15,
        paddingBottom: 15
    },
    redlabel: {
        color: 'rgba(51, 54, 64, 1)',
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 15
    },
    left: {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginBottom: 8,
    },
    right: {
        width: '50%',
        alignItems: 'flex-end',
        justifyContent: 'center',
        marginBottom: 8,
    },
    bodyLeft: {
        flexDirection: 'row',
    },
    bodyRight: {
        flexDirection: 'row'
    },
    bodyLabel: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 12,
        paddingBottom: 7,
        color: 'rgba(122, 130, 153, 1)'
    },
    bodyValue: {
        fontFamily: 'IRANSansMobile(FaNum)',
        fontSize: 14,
        color: 'black',
        paddingBottom: 7
    },
    address: {
        width: '30%',
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        marginBottom: 8,
    },
    redButton: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgb(92, 158, 255)',
        padding: 12,
        marginTop: 15,
        borderRadius: 7,
        elevation: 5
    },
    buttonText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold',
        fontFamily: 'IRANSansMobile(FaNum)'
    },
    roulsContainer: {
        width: '100%',
        flexDirection: 'row',
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: 'white',
        padding: 12,
        // marginTop: 15,
        borderRadius: 7,
        elevation: 5,
        marginBottom: 40
    },
    roulsText: {
        fontSize: 11,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        width: '90%',
        textAlign: 'right'
    },
    offeredContainer: {
        width: '100%',
        backgroundColor: 'white',
        borderRadius: 7,
        elevation: 5,
        // flexDirection: 'column',
        // alignItems: 'center',
        // justifyContent: 'center',
        // height: 270,
        paddingLeft: 15,
        paddingTop: 15,
        paddingBottom: 15,
    },
    insListContainer: {
        // flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        // height: 200,

    },
    item: {
        width: '24%',
        // height: '11%',
        marginRight: 10,
        marginLeft: 10,
        marginTop: 10
        // marginBottom: 80
    },
    imageContainer: {
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        // padding: 20,
        width: 80,
        height: 80,
        borderRadius: 100,
        // elevation: 8,
        borderWidth: 2,
        borderTopColor: '#38d0e6',
        borderRightColor: '#38d0e6',
        borderLeftColor: '#24d1ba',
        borderBottomColor: '#24d1ba',
    },
    bodyImage: {
        width: 30,
        resizeMode: 'contain',
        tintColor: '#2ad1c7'
    },
    bodyyImage: {
        width: 30,
        resizeMode: 'contain',
        tintColor: '#2ad1c7'
    },
    label: {
        fontSize: 9,
        color: 'black',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10
    },
});

