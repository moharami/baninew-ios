import React, {Component} from 'react';
import { View, TouchableOpacity, Text, ScrollView, ImageBackground, Alert, Image, BackHandler, NetInfo, Dimensions, Linking, Platform, AsyncStorage} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux';
// import homeBG from '../../assets/headerBG.png'
import homeBG from '../../assets/bime_bani.png'
// import homeBG from '../../assets/bani_bg2.png'
import Axios from 'axios';
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import ProfileItem from "../../components/profileItem/index";
import Icon from 'react-native-vector-icons/dist/Feather';
import FIcon from 'react-native-vector-icons/dist/FontAwesome';
import FooterMenu from "../../components/footerMenu/index";
import AlertView from "../../components/modalMassage";
import bime1 from "../../assets/bime/1.png";
import bime2 from "../../assets/bime/2.png";
import bime3 from "../../assets/bime/3.png";
import bime4 from "../../assets/bime/4.png";
import bime5 from "../../assets/bime/5.png";
import bime6 from "../../assets/bime/6.png";
import bime7 from "../../assets/bime/7.png";
import mass from "../../assets/mssoliat.png";
import motor from "../../assets/motor.png";
import SoonModal from './soonModal'
import LinearGradient from 'react-native-linear-gradient'
import {connect} from 'react-redux';
import {store} from '../../config/store';
class InsuranceBuy extends Component {
    constructor(props){
        super(props);
        this.state = {
            text: '',
            login: true,
            loading: true,
            modalVisible: false,
            wifi: false,
            isConnected: false,
            logout: false,
            modalVisible2: false
        };
    }
    componentDidMount() { // B
        if (Platform.OS === 'android') {
            Linking.getInitialURL().then(url => {
                console.log('uuuurlll in appp  adddress  initial url getttting', url)
                if(url !== null){
                    Actions.insuranceBuy();
                }
            });
        } else {
            Linking.addEventListener('url', this.handleOpenURL);
        }
    }
    componentWillUnmount() {
        console.log('some thing unmount')
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress);
        Linking.removeEventListener('url', this.handleOpenURL);

        // BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    handleOpenURL = (event) => { // D
        // console.log('handleOpenURL  event.url', event.url)
        if(event.url !== null){
            Actions.insuranceBuy();
        }
    }
    logout = () => {
        Alert.alert(
            'خروج از اپلیکیشن',
            'آیا از  خروج خود مطمئن هستید؟',
            [
                {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
                {text: 'بله', onPress: () =>  this.setState({logout: true}, () => { BackHandler.exitApp();})},
            ]
        )
    }
    onBackPress() {
        console.log('some thing loged')
        this.logout()
        // this.setState({logout: false})
        // Alert.alert(
        //     'خروج از اپلیکیشن',
        //     'آیا از  خروج خود مطمئن هستید؟',
        //     [
        //         {text: 'خیر', onPress: () => this.setState({logout: false}), style: 'cancel'},
        //         {text: 'بله', onPress: () =>  this.setState({logout: true}, () => { BackHandler.exitApp();})},
        //     ]
        // )
        return true;
    };
    componentWillUpdate(){
        BackHandler.addEventListener("hardwareBackPress",  this.onBackPress.bind(this));
    }
    componentWillMount() {
         // AsyncStorage.removeItem('token')
        // console.log('is nuuuuumber',/^[-]?\d+$/.test('08555fyfhfghf'))
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        NetInfo.getConnectionInfo().then((connectionInfo) => {
            console.log('heere is insBuy')
            AsyncStorage.getItem('token').then((info) => {
                if(info !== null) {
                    const newInfo = JSON.parse(info);
                    console.log('iiiiiiiiinfo', newInfo)
                    // this.setState({loading: true})
                    Axios.post('/user', {
                        mobile: newInfo.mobile
                    }).then(response=> {
                        this.setState({loading: false});

                        console.log('profile', newInfo.mobile)
                        store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                        this.setState({loading: false})
                    })
                        .catch((error) => {
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});
                        });
                }
                else {
                    this.setState({loading: false})
                }
            });
            this.setState({isConnected: connectionInfo.type !== 'none'}, ()=> {
                if(this.state.isConnected === false){
                    this.setState({modalVisible: true, loading: false, wifi: true})
                }
            })
        });

    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    closeModal2() {
        this.setState({modalVisible2: false});
    }
    render() {
        const {user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        }
        else return (
            <View style={styles.container}>
                <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)']} style={styles.liner}>
                    <View style={styles.top}>
                        <Text style={styles.headerTitle}>بانی بیمه</Text>
                        <TouchableOpacity onPress={() =>this.props.openDrawer()}>
                            <FIcon name="bars" size={20} color="white" />
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
                <View style={styles.container}>
                    <View style={styles.scroll}>
                        {/*<ImageBackground source={homeBG} style={styles.image}>*/}
                            {/*<View style={styles.imageRow}>*/}
                                {/*/!*<View style={styles.profile}>*!/*/}
                                {/*/!*</View>*!/*/}
                                {/*/!*<TouchableOpacity onPress={() => Actions.drawerOpen()}>*!/*/}
                                    {/*/!*<Icon name="menu" size={30} color="white" style={{paddingRight: 15}} />*!/*/}
                                {/*/!*</TouchableOpacity>*!/*/}
                            {/*</View>*/}
                            {/*/!*<Image style={{position: 'absolute', top: 20, left: '39%'}}*!/*/}
                                   {/*/!*source={require('../../assets/hmLogo.png')}/>*!/*/}
                        {/*</ImageBackground>*/}

                        <View style={styles.body}>
                            <View style={styles.row}>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(32, 52, 191)', 'rgb(61, 114, 224)',  'rgb(90, 181, 259)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, fire: true, pageTitle: 'بیمه آتش سوزی'})} >
                                            <View>
                                                <Image source={bime5} style={styles.bodyImage}  />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه آتش سوزی</Text>
                                </View>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(34, 249, 274)', 'rgb(41, 205, 254)',  'rgb(32, 156, 224)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, bodyBime: true, pageTitle: 'بیمه بدنه خودرو'})} >
                                            <View>
                                                <Image source={bime6} style={styles.bodyImage} />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه بدنه خودرو</Text>
                                </View>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(32, 52, 191)', 'rgb(61, 114, 224)',  'rgb(90, 181, 259)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true,motor: false, pageTitle: 'بیمه شخص ثالث'})} >
                                            <View>
                                                <Image source={bime1} style={styles.bodyImage} />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه شخص ثالث</Text>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(34, 249, 274)', 'rgb(41, 205, 254)',  'rgb(32, 156, 224)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity  onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, individual: true, pageTitle: 'بیمه درمان انفرادی'})} >
                                            <View>
                                                <Image source={bime3} style={styles.bodyImage} />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه درمان انفرادی</Text>
                                </View>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(32, 52, 191)', 'rgb(61, 114, 224)',  'rgb(90, 181, 259)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, medical: true, pageTitle: 'بیمه مسئولیت پزشکی'})} >
                                            <View>
                                                <Image source={bime2} style={styles.bodyImage} />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه مسئولیت پزشکی</Text>
                                </View>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(34, 249, 274)', 'rgb(41, 205, 254)',  'rgb(32, 156, 224)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, age: true, pageTitle: 'بیمه عمر'})} >
                                            <View>
                                                <Image source={bime4} style={styles.bodyImage} />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه عمر</Text>
                                </View>
                            </View>
                            <View style={styles.row}>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(32, 52, 191)', 'rgb(61, 114, 224)',  'rgb(90, 181, 259)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity onPress={() => Actions.otherVacleInfo({openDrawer: this.props.openDrawer, travel: true, pageTitle: 'بیمه مسافرتی'})} >
                                            <View>
                                                <Image source={bime7} style={styles.bodyImage} />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه مسافرتی</Text>
                                </View>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(34, 249, 274)', 'rgb(41, 205, 254)',  'rgb(32, 156, 224)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity onPress={() => Actions.vacleInfo({openDrawer: this.props.openDrawer, thirdBime: true, motor: true, pageTitle: 'بیمه موتور سیکلت'})}>
                                            <View>
                                                <Image source={motor} style={styles.bodyImage} />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه موتور سیکلت</Text>
                                </View>
                                <View style={styles.item}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgb(32, 52, 191)', 'rgb(61, 114, 224)',  'rgb(90, 181, 259)']}
                                                    style={styles.imageContainer}>
                                        <TouchableOpacity onPress={()=> this.setState({modalVisible2: true})} >
                                            <View>
                                                <Image source={mass} style={[styles.bodyImage, {width: 40}]} />
                                            </View>
                                        </TouchableOpacity>
                                    </LinearGradient>
                                    <Text style={styles.label}>بیمه مسئولیت مشاغل</Text>
                                </View>
                            </View>
                            <AlertView
                                closeModal={(title) => this.closeModal(title)}
                                modalVisible={this.state.modalVisible}
                                onChange={() => this.setState({modalVisible: false})}
                                title={(this.state.wifi ? 'لطفا وضعیت وصل بودن به اینترنت را بررسی کنید': 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید')}
                            />
                            <SoonModal
                                closeModal={(title) => this.closeModal2(title)}
                                onChange={(visible) => this.setState({modalVisible2: false})}
                                modalVisible={this.state.modalVisible2}
                            />
                        </View>
                    </View>
                    <FooterMenu active="bime" openDrawer={this.props.openDrawer}/>
                    {/*<View style={[styles.TrapezoidStyle, {borderRightWidth: Dimensions.get('window').width}]} />*/}
                </View>
            </View>

        );
    }
}
// export default InsuranceBuy;
function mapStateToProps(state) {
    return {
        user: state.auth.user,

    }
}
export default connect(mapStateToProps)(InsuranceBuy);