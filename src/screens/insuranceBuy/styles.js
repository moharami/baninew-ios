import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        flex: 1,
        position: 'relative',
        zIndex: 0
    },
    image: {
        width: '100%',
        height: '32%'
    },
    TrapezoidStyle: {
        flex: 1,
        position: 'absolute',
        top: 120,
        right: 0,
        left: 0,
        zIndex: 100,

        borderTopWidth: 50,
        borderLeftWidth: 0,
        borderBottomWidth: 0,
        borderTopColor: 'rgb(61, 99, 223)',
        borderRightColor: 'rgb(233, 233, 233)',
        borderLeftColor: 'transparent',
        borderBottomColor: 'transparent',
    },
    liner: {
        position: 'absolute',
        width: '100%',
        top: 0,
        flex: 1,
        zIndex: 999
    },
    top: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        height: 90,
        // paddingTop: 30,
        // paddingBottom: 30,
        paddingRight: 10
    },
    headerTitle: {
        color: 'white',
        fontSize: 16,
        fontFamily: 'IRANSansMobile(FaNum)',
        paddingRight: '34%'
    },
    imageRow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
    },
    profileImage: {
        width: 85,
        height: 85,
        borderRadius: 85,
    },
    profile: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 30,
        paddingBottom: 7,
        paddingTop: 20

    },
    imageContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 100,
        elevation: 8,
        width: 85,
        height: 85,

    },
    name: {
        fontSize: 13,
        color: 'rgb(255, 255, 255)',
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10,
        paddingLeft: 20,
        paddingBottom: 5
    },
    item: {
        width: '33%',
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 15
    },
    bodyImage: {
        width: 40,
        resizeMode: 'contain',
        tintColor: 'white'
    },
    bodyyImage: {
        width: 40,
        resizeMode: 'contain',
        tintColor: 'white'
    },
    scroll: {

        flex: 1,
        marginBottom: 80,
        paddingTop: 60
    },
    body: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingRight: 20,
        paddingLeft: 20,
        // paddingBottom: '63%',
        paddingTop: 2,
        // backgroundColor: 'red'
    },
    row: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    lastrow: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    iconContainer: {
        width: 30,
        height: 30,
        borderRadius: 30,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    label: {
        fontSize: 12,
        fontFamily: 'IRANSansMobile(FaNum)',
        textAlign: 'center',
        paddingTop: 10
    },

});
