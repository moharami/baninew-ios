import React, {Component} from 'react';
import {
    View,
    TouchableOpacity,
    Text,
    TextInput,
    KeyboardAvoidingView,
    Image,
    AsyncStorage,
    BackHandler,
    Alert
} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import LinearGradient from 'react-native-linear-gradient';
import Axios from 'axios'
// import ScalingDrawer from '../../components/react-native-scaling-drawer'
import PersianCalendarPicker from 'react-native-persian-calendar-picker';
;
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import Sidebar from "../../sidebar/index";
import moment from 'moment'
import {store} from '../../config/store';
import AlertView from '../../components/modalMassage'

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: '',
            loading: false,
            fname: '',
            lname: '',
            email: '',
            mobile: '',
            birthday: '',
            selectedStartDate: null,
            emailCorrect: false,
            showPicker: false,
            signed: false,
            modalVisible: false

        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        if(this.props.cantBack) {
            Actions.insuranceBuy({openDrawer: this.props.openDrawer})
            return true;
        }
        else{
            Actions.pop({refresh: {refresh: Math.random()}});
            return true;
        }
    };
    componentWillMount() {
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
    }
    closeModal() {
        this.setState({modalVisible: false});
    }
    signUp() {
        // const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        // if (reg.test(this.state.email) === true){
        //     this.setState({emailCorrect: true})
        // }
        // if(this.state.fname.length < 2 || this.state.lname.length < 2  ||  this.state.mobile.length !== 11 ){
        //     alert('لطفا داده ها را درست وارد کنید');
        // }
        // else{
            // console.log( moment(this.state.selectedStartDate).format('Y-M-D'));
            this.setState({loading: true});
            Axios.post('/set_profile', {
                fname: this.state.fname,
                lname: this.state.lname,
                mobile: this.state.mobile,
                email: this.state.email
            }).then(response=> {
                this.setState({loading: false});
                console.log('signup info', response);
                if(response.data.msg === 'ProfileSuccess'){
                    // Alert.alert('','ثبت نام شما با موفقیت انجام شد');
                    this.setState({signed: true, modalVisible: true, loading: false});

                    AsyncStorage.getItem('token').then((info) => {
                        const newInfo = JSON.parse(info);
                        Axios.post('/user', {
                        mobile: newInfo.mobile
                        }).then(response=> {
                            this.setState({loading: false});
                            console.log('profile', newInfo.mobile)
                            store.dispatch({type: 'USER_INFO_FETCHED', payload: response.data.data});
                            if(this.props.insBuy) {
                                Actions.prices({insBuy: true, openDrawer: this.props.openDrawer, factor:this.props.factor, user_details:this.props.user_details, insurType:this.props.insurType, instalment: this.props.instalment})
                            }
                            else if(this.props.profile) {
                                Actions.profile({openDrawer: this.props.openDrawer})
                            }
                        })
                        .catch((response) => {
                            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                            // this.setState({loading: false});
                            this.setState({modalVisible: true, loading: false});

                        });
                    });
                }
                else {
                    Actions.push('login')
                }
                // if(response.data.data === false){
                //     Actions.profile({mobile: this.state.text});
                // }
                // else {
                //     Actions.confirmation({mobile: this.state.text});
                // }
            })
            .catch((error) => {
                // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});

            });
        // }
    }
    // onDateChange(date) {
    //     setTimeout(() => {this.setState({showPicker: false})}, 200)
    //     this.setState({ selectedStartDate: date });
    //
    // }
    render() {
        return (
            <KeyboardAvoidingView style={styles.container}  behavior="padding" enabled>
            {/*<ScalingDrawer*/}
                {/*// ref={ref => this._drawer = ref}*/}
                {/*content={<Sidebar />}*/}
                {/*onClose={() => console.log('close')}*/}
                {/*onOpen={() => console.log('open')}*/}
            {/*>*/}
                {this.state.loading ? <Loader send={false}/> :

                        <View style={styles.send}>
                            <Image style={{alignSelf: 'flex-end', width: '40%'}} resizeMode={'contain'}
                                   source={require('../../assets/logo/small.png')}/>
                            <View style={styles.body}>
                                <Text style={styles.header}>خوش آمدید</Text>
                                <Text style={styles.label}>برای بهره مندی از خدمات بانی بیمه لطفا وارد شوید</Text>
                                <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                    <TextInput
                                        placeholder="نام"
                                        placeholderTextColor={'#C8C8C8'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.fname}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: '#C8C8C8',
                                            height: 45,
                                            backgroundColor: 'white',
                                            paddingRight: 15,
                                            flex: .9,
                                            borderTopLeftRadius: 6,
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: .1,
                                            borderTopRightRadius: 1,
                                            fontSize: 18,
                                            color: '#7A8299',
                                            fontFamily: 'IRANSansMobile(FaNum)'

                                        }}
                                        onChangeText={(text) => this.setState({fname: text})}/>
                                    <View style={{
                                        backgroundColor: '#C8C8C8',
                                        borderTopRightRadius: 6,
                                        borderBottomRightRadius: 6,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        height: 45
                                    }}>
                                        <Icon name={'user'} style={{fontSize: 23, paddingRight: 15, paddingLeft: 15}}/>
                                    </View>
                                </View>
                                <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                    <TextInput
                                        placeholder="نام خانوادگی"
                                        placeholderTextColor={'#C8C8C8'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.lname}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: '#C8C8C8',
                                            height: 45,
                                            backgroundColor: 'white',
                                            paddingRight: 15,
                                            flex: .9,
                                            borderTopLeftRadius: 6,
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: .1,
                                            borderTopRightRadius: 1,
                                            fontSize: 18,
                                            color: '#7A8299',
                                            fontFamily: 'IRANSansMobile(FaNum)'
                                        }}
                                        onChangeText={(text) => this.setState({lname: text})}/>
                                    <View style={{
                                        backgroundColor: '#C8C8C8',
                                        borderTopRightRadius: 6,
                                        borderBottomRightRadius: 6,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        height: 45
                                    }}>
                                        <Icon name={'user'} style={{fontSize: 23, paddingRight: 15, paddingLeft: 15}}/>
                                    </View>
                                </View>
                                <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                    <TextInput
                                        placeholder="ایمیل"
                                        placeholderTextColor={'#C8C8C8'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.email}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: '#C8C8C8',
                                            height: 45,
                                            backgroundColor: 'white',
                                            paddingRight: 15,
                                            flex: .9,
                                            borderTopLeftRadius: 6,
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: .1,
                                            borderTopRightRadius: 1,
                                            fontSize: 18,
                                            color: '#7A8299',
                                            fontFamily: 'IRANSansMobile(FaNum)'
                                        }}
                                        onChangeText={(text) => this.setState({email: text})}/>
                                    <View style={{
                                        backgroundColor: '#C8C8C8',
                                        borderTopRightRadius: 6,
                                        borderBottomRightRadius: 6,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        height: 45
                                    }}>
                                        <Icon name={'envelope'}
                                              style={{fontSize: 18, paddingRight: 15, paddingLeft: 15}}/>
                                    </View>
                                </View>
                                {/*<View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>*/}
                                    {/**/}
                                    {/*<TextInput*/}
                                        {/*onFocus={() => this.setState({showPicker: true})}*/}
                                        {/*placeholder="تاریخ تولد"*/}
                                        {/*placeholderTextColor={'#C8C8C8'}*/}
                                        {/*underlineColorAndroid='transparent'*/}
                                        {/*value={this.state.selectedStartDate !== null ? moment(this.state.selectedStartDate).format('Y-M-D') : null}*/}
                                        {/*style={{*/}
                                            {/*textAlign: 'right',*/}
                                            {/*borderWidth: 1,*/}
                                            {/*borderColor: '#C8C8C8',*/}
                                            {/*height: 45,*/}
                                            {/*backgroundColor: 'white',*/}
                                            {/*paddingRight: 15,*/}
                                            {/*flex: .9,*/}
                                            {/*borderTopLeftRadius: 6,*/}
                                            {/*borderBottomLeftRadius: 6,*/}
                                            {/*borderBottomRightRadius: .1,*/}
                                            {/*borderTopRightRadius: 1,*/}
                                            {/*fontSize: 18,*/}
                                            {/*color: '#7A8299'*/}
                                        {/*}}*/}
                                        {/*// onChangeText={(text) => this.setState({birthday: text})}*/}
                                    {/*/>*/}
                                    {/*<View style={{*/}
                                        {/*backgroundColor: '#C8C8C8',*/}
                                        {/*borderTopRightRadius: 6,*/}
                                        {/*borderBottomRightRadius: 6,*/}
                                        {/*alignItems: 'center',*/}
                                        {/*justifyContent: 'center',*/}
                                        {/*height: 45*/}
                                    {/*}}>*/}
                                        {/*<Icon name={'calendar-o'}*/}
                                              {/*style={{fontSize: 19, paddingRight: 15, paddingLeft: 15}}/>*/}
                                    {/*</View>*/}
                                {/*</View>*/}



                                <View style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                                    <TextInput
                                        placeholder="شماره موبایل"
                                        keyboardType='numeric'
                                        placeholderTextColor={'#C8C8C8'}
                                        underlineColorAndroid='transparent'
                                        value={this.state.mobile}
                                        style={{
                                            textAlign: 'right',
                                            borderWidth: 1,
                                            borderColor: '#C8C8C8',
                                            height: 45,
                                            backgroundColor: 'white',
                                            paddingRight: 15,
                                            flex: .9,
                                            borderTopLeftRadius: 6,
                                            borderBottomLeftRadius: 6,
                                            borderBottomRightRadius: .1,
                                            borderTopRightRadius: 1,
                                            fontSize: 18,
                                            color: '#7A8299',
                                            fontFamily: 'IRANSansMobile(FaNum)'
                                        }}
                                        onChangeText={(text) => this.setState({mobile: text})}/>
                                    <View style={{
                                        backgroundColor: '#C8C8C8',
                                        borderTopRightRadius: 6,
                                        borderBottomRightRadius: 6,
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        height: 45
                                    }}>
                                        <Icon name={'mobile-phone'}
                                              style={{fontSize: 30, paddingRight: 17, paddingLeft: 17}}/>
                                    </View>
                                </View>


                                <TouchableOpacity onPress={() => this.signUp()} style={styles.advertise}>
                                    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}}
                                                    colors={['#3CB1E8', '#3E40DB']}
                                                    style={styles.advertise}>
                                        <View>
                                            <Text style={styles.buttonTitle}>ثبت نام</Text>
                                        </View>
                                    </LinearGradient>
                                </TouchableOpacity>

                            </View>
                            {/*{*/}
                                {/*this.state.showPicker ?*/}
                                    {/*<View style={{*/}
                                        {/*position: 'absolute',*/}
                                        {/*bottom: '35%',*/}
                                        {/*zIndex: 9999,*/}
                                        {/*backgroundColor: 'white'*/}
                                    {/*}}>*/}
                                        {/*<PersianCalendarPicker*/}
                                            {/*onDateChange={(date) => this.onDateChange(date)}*/}
                                        {/*/>*/}
                                    {/*</View>*/}
                                    {/*: null*/}
                            {/*}*/}
                        </View>



                }
                <Image style={{height: '20%', zIndex: -1, width: '100%', position: 'absolute', bottom: 0}}
                       resizeMode={'cover'} source={require('../../assets/company-hero-3.png')}/>
                <AlertView
                    closeModal={(title) => this.closeModal(title)}
                    modalVisible={this.state.modalVisible}
                    onChange={(visible) => this.setModalVisible(visible)}

                    title={this.state.signed ? 'ثبت نام شما با موفقیت انجام شد' : 'مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'}
                />
            {/*</ScalingDrawer>*/}
            </KeyboardAvoidingView>
        );
    }
}

export default Register;