
import React, {Component} from 'react';
import {View, TouchableOpacity, ScrollView, Text, StatusBar, SafeAreaView, AsyncStorage, BackHandler, Alert, Image, TextInput} from 'react-native';
import styles from './styles'
import {Actions} from 'react-native-router-flux'
import FIcon from 'react-native-vector-icons/dist/Feather';
import SIcon from 'react-native-vector-icons/dist/SimpleLineIcons';
import {store} from '../../config/store';
import {connect} from 'react-redux';
import Axios from 'axios'
;
export const url = 'https://banibime.com/api/v1';
Axios.defaults.baseURL = url;
import Loader from '../../components/loader'
import HomeHeader from "../../components/homeHeader/index";
import PictureInfo from '../../components/pictureInfo'
import TimeItem from "../../components/timeItem/index";
import AlertView from '../../components/modalMassage'
import LinearGradient from 'react-native-linear-gradient';

class TimeSelecting extends Component {
    constructor(props){
        super(props);
        this.state = {
            loading: true,
            status: 1,
            activeButton: 1,
            activeVacle: 1,
            showPicker: false,
            selectedStartDate: null,
            nextStep: false,
            times: [],
            part1: [],
            part2: [],
            partDate: [],
            visible: false,
            activeId: null,
            modalVisible: false
        };
        this.onBackPress = this.onBackPress.bind(this);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }
    onBackPress() {
        Actions.pop({refresh: {refresh:Math.random()}});
        return true;
    };
    closeModal() {
        this.setState({modalVisible: false});
    }
    componentWillMount(){
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        this.setState({loading: true});
        Axios.get('/request/get-date').then(response => {
            let newArray = [];
            let newArrayDate = [];
            const timeArray = Object.values(response.data)
            console.log('timeArray', timeArray)
            for(let i=0 ; i<timeArray.length ; i++){
                if(i % 2 === 0){
                    if(timeArray[i+1].isHoliday === false){
                        newArray.push(timeArray[i])
                        newArrayDate.push(timeArray[i+1].gdate)
                    }
                }
            }
            // const filterdData = timeArray.filter((item) => item !== true )
            // console.log('filtered data', filterdData)
            const part1 = [newArray[0], newArray[1], newArray[2]];
            const part2 = [];
            for(let i=3 ; i<newArray.length; i++) {
                part2.push(newArray[i])
            }
            this.setState({loading: false, times: newArray, part1: part1, part2: part2, partDate: newArrayDate});
        })
        .catch((error) =>{
            console.log(error)
            // Alert.alert('','خطایی رخ داده مجددا تلاش نمایید');
            // this.setState({loading: false});
            this.setState({modalVisible: true, loading: false});

        });

    }
    check(time, id) {
        console.log('time', time);
        let factor = this.props.user_details;
        this.props.user_details.date_0=this.state.times[0];
        this.props.user_details.date_1=this.state.times[1];
        this.props.user_details.date_2=this.state.times[2];
        this.props.user_details.time_delivery=time;

        console.log(factor);
        this.setState({activeId: id, nextStep:true})
    }
    nextStep() {
        if(this.state.nextStep) {
            Actions.finalConfirm({openDrawer: this.props.openDrawer, pageTitle: 'تایید نهایی', factor:this.props.factor, user_details:this.props.user_details, images: this.props.images, insurType:this.props.insurType,Birth:this.props.Birth, instalment: this.props.instalment});
        }
    }
    render() {
        const times = this.state.times;
        // const {posts, categories, user} = this.props;
        if(this.state.loading){
            return (<Loader />)
        } else
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.header}>
                    <HomeHeader active={5} openDrawer={this.props.openDrawer} pageTitle={this.props.pageTitle} />
                </View>
                <ScrollView style={styles.scroll}>
                    <View style={styles.bodyContainer}>
                        <View style={styles.labelContainer}>
                            <Text style={styles.topLabel}>انتخاب زمان</Text>
                            <FIcon name="clock" size={20} color="gray" />
                        </View>
                        <View style={styles.body}>
                            {
                                this.state.part1 && this.state.part1.map((item, index) => <TimeItem check={(time, id) => this.check(time, id)} item={item} part2={false} key={index} id={index} activeId={this.state.activeId}  />)
                            }
                        </View>
                        <TouchableOpacity onPress={() => this.setState({visible: !this.state.visible})}>
                            <Text style={styles.seeMore}>مشاهده روزهای آتی</Text>
                        </TouchableOpacity>
                        {
                            this.state.visible ?
                                <View style={styles.body}>
                                    {
                                        this.state.part2 && this.state.part2.map((item, index) => <TimeItem check={(time, id) => this.check(time, id)} item={item} key={index} part2={true} partDate={this.state.partDate} id={index+3} activeId={this.state.activeId}  />)
                                    }
                                </View>
                                : null
                        }
                        <AlertView
                            closeModal={(title) => this.closeModal(title)}
                            modalVisible={this.state.modalVisible}
                            // onChange={(visible) => this.setModalVisible(visible)}

                            title='مشکلی در برقراری ارتباط با سرور به وجود آمده لطفا دوباره سعی کنید'
                        />
                    </View>
                </ScrollView>
                <View style={styles.footer}>
                    <TouchableOpacity style={[styles.iconLeftContainer, {backgroundColor: this.state.nextStep ? 'rgba(255, 193, 39, 1)' : 'rgba(200, 200, 200, 1)' }]} onPress={() => this.nextStep()}>
                        <FIcon name="arrow-left" size={18} color="white" style={{borderColor: 'white', borderWidth: 1, borderRadius: 20, padding: 3, marginRight: 5}} />
                        <Text style={styles.label}>بعدی</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.iconRightContainer} onPress={() => this.onBackPress()}>
                        <FIcon name="arrow-right" size={22} color="rgba(17, 103, 253, 1)" />
                    </TouchableOpacity>
                </View>
                <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)']} style={styles.topBlue} />
            </SafeAreaView>
        );
    }
}
export default TimeSelecting;

