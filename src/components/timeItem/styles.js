

import { StyleSheet } from 'react-native';
// import env from '../../colors/env';

export default StyleSheet.create({
    container: {
        // flex: .5,
        width: '48%',
        alignItems: "center",
        justifyContent: 'center',
        backgroundColor: 'white',
        // paddingRight: 30,
        // paddingLeft: 30,
        // paddingBottom: 20,
        // paddingTop: 20,
        borderRadius: 10,
        elevation: 5,
        marginRight: 5,
        marginBottom: 10
    },
    left: {
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-start',
    },
    label: {
        color: 'black',
        fontSize: 14,
        fontFamily: 'IRANSansMobile(FaNum)',
        padding: 3
    },
    allTime: {
        width: '100%',
        borderTopWidth: 1,
        borderTopColor: 'rgb(237, 237, 237)',
        alignItems: "flex-end",
        justifyContent: 'center'
    },
    timeContainer: {
        // width: '70%',
        width: '100%',
        flexDirection: 'row',
        alignItems: "center",
        justifyContent: 'flex-end',
        paddingLeft: 20

    }
});

