import React, {Component} from 'react';
import {TextInput, View, Text, TouchableOpacity, Picker} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { CheckBox } from 'react-native-elements'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import Axios from 'axios';
export const url = 'https://bimat.ir/api/v1';
Axios.defaults.baseURL = url;
import Selectbox from 'react-native-selectbox'
import SelectInput from 'react-native-select-input-ios'

class UserItem extends Component {

    constructor(props){
        super(props);
        this.state = {
            text: '',
            provinces: [],
            province: 0,
            loading: true,
            checked: this.props.status === 'female' ? 2 : (this.props.status === 'male' ? 1 : 0),
            visibleinput2: false,
            // birthdayYear: 0,
            // birthdayDay: 0,
            // birthdayMonth: 0,
            birthdayYear: this.props.birthdayYear !== undefined ? this.props.birthdayYear : 0,
            birthdayMonth:this.props.birthdayMonth !== undefined ? this.props.birthdayMonth : 0,
            birthdayDay:this.props.birthdayDay !== undefined ? this.props.birthdayDay : 0,
        };
    }
    handleChange(text){
        if(this.props.label !== 'شماره موبایل') {
            this.setState({text: text});
            this.props.check(text);
        }
    }
    componentWillMount(){
        console.log('herrrrre ', this.props.birthday)
        console.log('yearArray', this.props.birthdayYear )
        console.log('monthArray', this.props.birthdayMonth )
        console.log('adyArray', this.props.birthdayDay )

        Axios.get('/request/get_insurance').then(response => {
            this.setState({insurances: response.data.data});
            Axios.post('/request/get-province', {}).then(response => {
                this.setState({provinces: response.data.data, loading: false});
            })
                .catch((error) => {
                    // Alert.alert('','خطا')
                    // this.setState({loading: false});
                    this.setState({modalVisible: true, loading: false});
                    console.log(error);
                });
        })
            .catch((error) => {
                // Alert.alert('','خطا')
                // this.setState({loading: false});
                this.setState({modalVisible: true, loading: false});
                console.log(error);
            });
    }
    focuse() {
        if(this.props.birthday) {
            this.props.openPicker();
        }
    }
    onShowinput2 = () => {
        this.setState({ visibleinput2: true });
    }

    onSelectInput2 = (picked) => {
        this.setState({
            province: picked,
            visibleinput2: false
        }, () =>{
            this.state.provinces.map((item) => {
                if(picked === item.id) {
                    this.setState({text: item.name}, () =>  this.handleChange(item.name));
                }
            })
        })
    }
    onCancelinput2 = () => {
        this.setState({
            visibleinput2: false
        })
    }
    test() {

        if(this.state.birthdayYear !== 0 && this.state.birthdayDay !== 0 && this.state.birthdayMonth !== 0 ) {
            const birth = this.state.birthdayYear +'-' + this.state.birthdayMonth +'-' + this.state.birthdayDay;
            this.props.check(birth);
        }
    }
    render() {
        // let yearArray = [],  monthArray= [], dayArray = [];
        let yearArray = [{key: 0, label: this.props.birthdayYear !== undefined ? this.props.birthdayYear : 'سال', value: this.props.birthdayYear !== undefined ? this.props.birthdayYear : 0 }], monthArray = [{key: 0, label: this.props.birthdayMonth !== undefined ? this.props.birthdayMonth : 'ماه', value: this.props.birthdayMonth !== undefined ? this.props.birthdayMonth : 0}], dayArray = [{key: 0, label: this.props.birthdayDay !== undefined ? this.props.birthdayDay : 'روز', value: this.props.birthdayDay !== undefined ? this.props.birthdayDay : 0}];

        if(this.props.birthday) {
            for(let i=1397, j= 0; i>=1300 ; i--, j++){
                yearArray.push({key: j, label: i.toString(), value: i})
            }
            const month = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12"]
            monthArray = month.map((item)=> {return {key: parseInt(item), label: item, value: item}})
            const day = ["01", "02", "03", "04", "05", "06", "07", "08", "09","10", "11","12", "13", "14", "15", "16", "17", "18", "19", "20", "21","22", "23","24", "25", "26", "27", "28", "29", "30", "31"];
            dayArray = day.map((item)=> {return {key: parseInt(item), label: item, value: item}})

        }



        // const {posts, categories, user} = this.props;
        return (
            <View style={[styles.container, {borderBottomColor: this.props.border ? 'rgb(237, 237, 237)' : 'transparent', borderBottomWidth: this.props.border ? 1 : 0,borderRadius: 10}]}>
                <View style={styles.left}>
                    {/*<Icon name="chevron-left" size={12} color="rgb(180, 180, 180)" style={{paddingRight: 30}} />*/}
                    {
                        this.props.sex ?
                            <View style={styles.allTime}>
                                <TouchableOpacity onPress={() =>  {this.setState({ checked: 1}, ()=> {this.props.check('male')})}} style={styles.timeContainer}>
                                    <Text style={styles.label}>مرد</Text>
                                    <CheckBox
                                        containerStyle={{backgroundColor: 'transparent', borderWidth: 0, padding: 0}}
                                        // center
                                        // title='Click Here'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={this.state.checked === 1}
                                        onPress={() =>  {this.setState({ checked: 1}, ()=> {this.props.check('male')})}} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() =>  {this.setState({ checked: 2}, ()=> {this.props.check('female')})}}  style={styles.timeContainer}>
                                    <Text style={styles.label}>زن</Text>
                                    <CheckBox
                                        containerStyle={{backgroundColor: 'transparent', borderWidth: 0, padding: 0}}
                                        // center
                                        // title='Click Here'
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        checked={this.state.checked === 2}
                                        onPress={() =>  {this.setState({ checked: 2}, ()=> {this.props.check('female')})}} />
                                </TouchableOpacity>
                            </View> :
                            this.props.province ?
                                <TouchableOpacity onPress={this.onShowinput2} style={{position: 'relative', zIndex: 3, width: 180, backgroundColor: 'white', height: 32, borderColor: this.state.redBorder && this.state.province === 0 ? 'red' : 'transparent', borderWidth: 1, borderRadius: 10}}>
                                    {/*<FIcon name="chevron-down" size={20} color="gray" style={{position: 'absolute', zIndex: 90, top: 10, left: 10}}/>*/}
                                    <Text style={{paddingTop: 5, paddingRight: 10,  fontFamily: 'IRANSansMobile(FaNum)', color: 'gray' ,fontSize: 12, textAlign: 'right' }}>{this.state.loading ? "در حال بارگذاری..." : this.state.province === 0 ? "انتخاب نشده" : this.state.text}</Text>
                                    <ModalFilterPicker
                                        visible={this.state.visibleinput2}
                                        onSelect={this.onSelectInput2}
                                        onCancel={this.onCancelinput2}
                                        options={  this.state.provinces.map((item) => {return {key: item.id, label: item.name, value: item.id}})}
                                        placeholderText="جستجو ..."
                                        cancelButtonText="لغو"
                                        filterTextInputStyle={{textAlign: 'right'}}
                                        optionTextStyle={{textAlign: 'right', width: 180}}
                                        titleTextStyle={{textAlign: 'right'}}
                                    />
                                </TouchableOpacity>
                                :
                            this.props.birthday ?
                                <View style={{width: 220, flexDirection: 'row', padding: 3, zIndex: 3,  height: 42, backgroundColor: 'white', }}>
                                    <SelectInput
                                        value={this.state.birthdayYear}
                                        options={yearArray}
                                        onCancelEditing={() => console.log('onCancel')}
                                        submitKeyText="انتخاب"
                                        cancelKeyText="لغو"
                                        labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 14}}
                                        pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                        buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                        onSubmitEditing={(itemValue) =>{
                                            console.log('itemValue for years', itemValue)
                                            this.setState({
                                                birthdayYear: itemValue
                                            }, ()=> this.test())}}
                                        // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                        style={{
                                            flexDirection: 'row',
                                            width: '30%',
                                            height: 42,
                                            padding: 8,
                                            backgroundColor: '#FFFFFF'
                                        }}
                                    />
                                    <SelectInput
                                        value={this.state.birthdayMonth}
                                        options={monthArray}
                                        onCancelEditing={() => console.log('onCancel')}
                                        submitKeyText="انتخاب"
                                        cancelKeyText="لغو"
                                        labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 14}}
                                        pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                        buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                        onSubmitEditing={(itemValue) =>{
                                            console.log('itemValue for years', itemValue)
                                            this.setState({
                                                birthdayMonth: itemValue
                                            }, ()=> {this.test(); console.log('itemValue', itemValue)})}}
                                        // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                        style={{
                                            flexDirection: 'row',
                                            width: '30%',
                                            height: 42,
                                            padding: 8,
                                            backgroundColor: '#FFFFFF'
                                        }}
                                    />
                                    <SelectInput
                                        value={this.state.birthdayDay}
                                        options={dayArray}
                                        onCancelEditing={() => console.log('onCancel')}
                                        submitKeyText="انتخاب"
                                        cancelKeyText="لغو"
                                        labelStyle={{color: 'rgb(150, 150, 150)', fontFamily: 'IRANSansMobile(FaNum)', fontSize: 14}}
                                        pickerItemsStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                        buttonsTextStyle={{fontFamily: 'IRANSansMobile(FaNum)', fontSize: 13}}
                                        onSubmitEditing={(itemValue) =>{
                                            console.log('itemValue for years', itemValue)
                                            this.setState({
                                                birthdayDay: itemValue
                                            }, ()=> this.test())}}
                                        // style={{height: 42, paddingRight: 10, width: '100%', backgroundColor: 'white'}}
                                        style={{
                                            flexDirection: 'row',
                                            width: '30%',
                                            height: 42,
                                            padding: 8,
                                            backgroundColor: '#FFFFFF'
                                        }}
                                    />
                                </View>
                                :
                                <TextInput
                                    keyboardType={(this.props.label === 'کد ملی' || this.props.label === 'کد پستی' || this.props.label === 'شماره کارت') ? 'numeric' : undefined}
                                    maxLength={this.props.label === 'کد پستی' ? 10 : this.props.label === 'شماره شبا'? 26 : this.props.label === 'نام' || this.props.label === 'نام پدر' || this.props.label === 'نام بانک' ? 12 : this.props.label === 'شماره موبایل' ? 11 : this.props.label === 'نام خانوادگی' ? 16 : 40}
                                    onFocus={() => this.focuse()}
                                    secureTextEntry={this.props.pass}
                                    placeholder={this.props.value}
                                    placeholderTextColor={'gray'}
                                    underlineColorAndroid='transparent'
                                    value={this.state.text}
                                    style={{
                                        height: 40,
                                        backgroundColor: 'white',
                                        paddingRight: 15,
                                        width: this.props.label === 'شماره شبا' ? 225 : 180,
                                        color: 'gray',
                                        fontSize: 14,
                                        textAlign: this.props.label === 'شماره شبا' ? 'center' : 'right',
                                    }}
                                    // onChangeText={(text) => this.setState({text: text, fill: ++this.state.fill})}
                                    onChangeText={(text) => this.handleChange(text)}
                                />
                    }

                </View>
                <Text style={styles.label}>{this.props.label}</Text>
            </View>
        );
    }
}
export default UserItem;