import React, {Component} from 'react';
import {TouchableOpacity, View, Text} from 'react-native';
import styles from './styles'
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import LinearGradient from 'react-native-linear-gradient';
import {Actions} from 'react-native-router-flux'
import Steps from "../steps/index";

class HomeHeader extends Component {

    constructor(props){
        super(props);
        this.state = {
        };
    }
    render() {
        return (
            <LinearGradient  start={{x: 0, y: 0}} end={{x: 1, y: 1}} colors={['rgba(60, 177, 232, 1)', 'rgba(62, 64, 219, 1)']} style={styles.container}>
                <View style={styles.top}>
                    <Text style={styles.headerTitle}>{this.props.pageTitle}</Text>
                    <TouchableOpacity onPress={() =>this.props.openDrawer()}>
                        <Icon name="bars" size={20} color="white" />
                    </TouchableOpacity>
                </View>
                <Steps active={this.props.active} />
            </LinearGradient>
        );
    }
}
export default HomeHeader;